﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC5StarterProject.Controllers
{
    public class HelloWorldController : Controller
    {
        //
        // GET: /HelloWorld/

        public string Index()
        {
            return "這是我的 <b>預設</b> 動作...";
        }

        //
        // GET: /HelloWorld/Welcome/

        public string Welcome()
        {
            return "這是歡迎的 action...";
        }
    }
}